package ru.spring.utils;

import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

public class FileUtil {
    public static File multipartToFile(MultipartFile multipart, String fileName) throws IllegalStateException, IOException {
        File temporaryFile = new File(System.getProperty("java.io.tmpdir") + "/" + fileName);
        multipart.transferTo(temporaryFile);
        return temporaryFile;
    }


    public static ByteArrayOutputStream inputStreamToOutputStream(S3Object s3object){
        try {
            S3ObjectInputStream inputStream = s3object.getObjectContent();
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            int len;
            byte[] buffer = new byte[4096];
            while ((len = inputStream.read(buffer, 0, buffer.length)) != -1) {
                outputStream.write(buffer, 0, len);
            }
            return outputStream;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
