package ru.spring.utils.constants;

public final class ExceptionConstants {
    private ExceptionConstants() {
    }

    public static final String NOT_FOUND_EXCEPTION = "Not found exception";
    public static final String INVALID_JWT_AUTH_EXCEPTION = "Invalid jwt authentication exception";
    public static final String JWT_TOKEN_EXPIRED_OR_INVALID = "JWT token is expired or invalid";
    public static final String VALIDATION_FAILED = "Validation failed";
    public static final String VALIDATION_EXCEPTION = "Validation exception";
    public static final String TRAILING_SLASH_FILTER_EXCEPTION = "An error occurred processing the request. Please try again.";
    public static final String JWT_TOKEN_IS_EXPIRED_OR_INVALID = "JWT token is expired or invalid";
    public static final String ERROR_PROCESSING_JSON = "Error processing JSON";
    public static final String USERNAME_NOT_FOUND_EXCEPTION = "Username not found exception";
    public static final String ILLEGAL_ARGUMENT_EXCEPTION = "Illegal argument exception";
    public static final String BAD_CREDENTIALS = "Bad credentials";
    public static final String INVALID_TYPE = "Invalid: Type -> %s";
    public static final String CAN_NOT_FOUND_STAGE_BY_ID = "Can't found stage by id - %d";
}
