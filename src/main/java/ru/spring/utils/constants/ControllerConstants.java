package ru.spring.utils.constants;

public final class ControllerConstants {
    private ControllerConstants() {
    }
    public static final String USER_NAME_IN_LINE = "username";
    public static final String TOKEN = "token";
    public static final String ROLE_ADMIN_FULL = "ROLE_ADMIN";
    public static final String ROLE_USER_FULL = "ROLE_USER";
    public static final String ROLE = "role";
    public static final String INVALID_USERNAME_PASSWORD_SUPPLIED = "Invalid username/password supplied";
    public static final String USERNAME_IS_ALREADY_EXIST = "Username \"%s\" is already exist!";
    public static final String EXCEPTION_IN_SIGN_UP = "Exception in signUp: ";
    public static final String RESET_PASSWORD = "Reset Password";
    public static final String HELLO_MESSAGE = "Hello, %s.\nYou received this email because you requested to reset your password.\nTo set a new password for your account, you have to follow the next link: ";
    public static final String LINK_FOR_USER = "http://localhost:4200/auth/changepassword?id=%d&token=%s";
    public static final String VALID = "valid";
    public static final String USER_WITH_USERNAME = "User with username: ";
    public static final String NOT_FOUND = " not found";
    public static final String NEW_LINE = " \r\n";
    public static final String TEXT = "text";
    public static final String NAME = "name";
    public static final String DESCRIPTION = "description";
    public static final String REQUIRED_SKILLS = "requiredSkills";
    public static final String MESSAGE_IDS = "message_ids";
    public static final String REQUIRED_SKILLS_MUST_BE_A_LIST_OF_STRINGS = "requiredSkills must be a list of strings";
    public static final String FROM_USER_TO_TEAM = "FROM_USER_TO_TEAM";
    public static final String FROM_USER_TO_JOB = "FROM_USER_TO_JOB";
    public static final String  UNDER_CONSIDERATION = "UNDER_CONSIDERATION";
    public static final String  SUBJECT = "subject";
    public static final String  THIS_EMAIL_FROM  = "...This email from: ";
    public static final String  UNABLE_TO_CREATE_PDF_FILE  = "Unable to create PDF file";
}
