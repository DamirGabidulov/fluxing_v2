package ru.spring.utils.constants;

public final class DtoConstants {
    private DtoConstants() {
    }
    public static final String SLASH = "/";
    public static final int ONE = 1;
    public static final double ZERO_ZERO_DOUBLE = 0.0d;
}
