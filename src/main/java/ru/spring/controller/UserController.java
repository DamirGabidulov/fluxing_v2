package ru.spring.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;
import ru.spring.dto.SignUpForm;
import ru.spring.dto.UserDto;
import ru.spring.exception.UnauthorizedException;
import ru.spring.model.Status;
import ru.spring.model.User;
import ru.spring.repository.UserRepository;
import ru.spring.service.UserService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/users")
public class UserController {

    private final UserService userService;
    private final UserRepository userRepository;

    @GetMapping
    public ResponseEntity<List<UserDto>> findAll() {
        List<UserDto> users = userService.findAll();
        if (users.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {
            return ResponseEntity.ok().body(users);
        }
    }

    @GetMapping("/{userId}")
    public ResponseEntity<?> getById(@PathVariable("userId") Integer userId) {
        try {
            return ResponseEntity.ok().body(userService.getById(userId));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(404).body(e.getMessage());
        }
    }


    @PostMapping("/signUp")
    public ResponseEntity<?> signUp(@RequestBody SignUpForm form) {
        try {
            return ResponseEntity.status(201).body(userService.save(form));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(404).body(e.getMessage());
        }
    }

    @PutMapping("/{userId}")
    public ResponseEntity<?> update(@PathVariable("userId") Integer userId, @RequestBody UserDto userDto) {
        if (userService.isUserExist(userId)) {
            return ResponseEntity.ok().body(userService.update(userId, userDto));
        } else {
            return new ResponseEntity<>("User with this id doesn't exist", HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{userId}")
    public ResponseEntity<?> delete(@PathVariable("userId") Integer userId) {
        if (userService.isUserExist(userId)) {
            userService.delete(userId);
            return new ResponseEntity<>("Deleted successful", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("User with this id doesn't exist", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/hhh/{id}")
    public Mono<UserDto> findByIdPlusFilter(@PathVariable("id") Integer id) {
        User user1 = userRepository.findById(id).stream()
                .filter(user -> user.getStatus().equals(Status.ACTIVE))
                .findFirst()
                .orElseThrow(() -> new UnauthorizedException("ok ok ok ok"));
        return Mono.just(UserDto.from(user1))
                .switchIfEmpty(Mono.error(new UnauthorizedException("User disabled")));
    }
}
