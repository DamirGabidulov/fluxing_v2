package ru.spring.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.spring.dto.EventDto;
import ru.spring.model.User;
import ru.spring.repository.UserRepository;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/events")
public class EventController {

    private final UserRepository userRepository;

    @GetMapping
    public ResponseEntity<List<EventDto>> findAll(@RequestHeader("userId") Integer userId){
        User user = userRepository.findById(userId).orElseThrow(() -> new IllegalArgumentException("User with this id doesn't exist"));
        List<EventDto> eventDtoList = EventDto.from(user.getEvents());
        if (eventDtoList.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {
            return ResponseEntity.ok().body(eventDtoList);
        }
    }
}
