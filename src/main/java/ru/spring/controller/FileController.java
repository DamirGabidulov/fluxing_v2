package ru.spring.controller;

import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.S3Object;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.spring.dto.FileDto;
import ru.spring.model.Event;
import ru.spring.model.File;
import ru.spring.service.FileService;
import ru.spring.service.S3Service;
import ru.spring.service.UserService;
import ru.spring.utils.FileUtil;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/files")
public class FileController {

    private final FileService fileService;
    private final S3Service s3Service;
    private final UserService userService;

    //TODO этот метод будет только для админа
    @GetMapping
    public ResponseEntity<List<FileDto>> findAll() {
        List<File> files = fileService.findAll();
        if (files.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.status(200).body(FileDto.from(files));
    }

    @GetMapping("/users/{userId}")
    public ResponseEntity<?> findAll(@PathVariable("userId") Integer userId) {
        if (userService.isUserExist(userId)) {
            List<File> files = userService.getById(userId).getEvents().stream().map(Event::getFile).collect(Collectors.toList());
            if (files.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            return ResponseEntity.status(200).body(FileDto.from(files));
        } else {
            return ResponseEntity.status(404).body("User with this id doesn't exist");
        }
    }

    @GetMapping(value = "/{fileId}")
    public ResponseEntity<?> getFile(@PathVariable("fileId") Integer fileId, @RequestHeader("userId") Integer userId) {
        if (userService.isFileOwnedByUser(userId, fileId)) {
            FileDto fileDto = fileService.getById(fileId);
            try {
                S3Object s3object = s3Service.getS3Object(fileDto.getLocation());
                String contentType = s3object.getObjectMetadata().getContentType();
                //переводим inputStream от чтения нашего файла в хранилище амазона в output для отображения в браузере
                ByteArrayOutputStream outputStream = FileUtil.inputStreamToOutputStream(s3object);

                return ResponseEntity.ok()
                        .contentLength(s3object.getObjectMetadata().getContentLength())
                        .contentType(contentType.isEmpty() ? MediaType.APPLICATION_OCTET_STREAM : MediaType.valueOf(contentType))
                        //inline - отображение в браузере, attachment - скачивание
                        .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + fileDto.getName() + "\"")
                        .body(outputStream.toByteArray());
            } catch (AmazonS3Exception e) {
                return ResponseEntity.status(404).body("Can't find file in storage. Server message " + e);
            }
        } else {
            return ResponseEntity.status(404).body("User with this id doesn't exist or file doesn't belong to user");
        }
    }

    @PostMapping
    public ResponseEntity<?> save(@RequestParam("file") MultipartFile file, @RequestHeader("userId") Integer userId) {
        if (userService.isUserExist(userId) & !userService.isFileWithThisNameAlreadyExist(file, userId)) {
            return ResponseEntity.status(202).body(fileService.save(file, userId));
        } else {
            return ResponseEntity.status(404).body("User with this id doesn't exist or file doesn't belong to user");
        }
    }

    @PutMapping(value = "/{fileId}")
    public ResponseEntity<?> update(@PathVariable("fileId") Integer fileDtoId,
                                    @RequestParam("file") MultipartFile file,
                                    @RequestHeader("userId") Integer userId) {
        if (userService.isFileOwnedByUser(userId, fileDtoId)) {
            return ResponseEntity.ok().body(fileService.update(fileDtoId, file, userId));
        } else {
            return ResponseEntity.status(404).body("User with this id doesn't exist or file doesn't belong to user");
        }
    }

    @DeleteMapping(value = "/{fileId}")
    public ResponseEntity<?> delete(@PathVariable("fileId") Integer fileDtoId, @RequestHeader("userId") Integer userId) {
        if (userService.isFileOwnedByUser(userId, fileDtoId)) {
            fileService.delete(fileDtoId, userId);
            return new ResponseEntity<>("Deleted successful", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("User with this id doesn't exist or file doesn't belong to user", HttpStatus.NOT_FOUND);
        }
    }
}
