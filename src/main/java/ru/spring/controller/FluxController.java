package ru.spring.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.spring.dto.CheckForm;
import ru.spring.dto.UserDto;
import ru.spring.exception.ApiException;
import ru.spring.exception.AuthException;
import ru.spring.exception.UnauthorizedException;
import ru.spring.model.Event;
import ru.spring.model.Status;
import ru.spring.model.User;
import ru.spring.repository.UserRepository;
import ru.spring.security.flux.SecurityService;
import ru.spring.service.UserService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/flux")
public class FluxController {

    private final UserService userService;
    private final UserRepository userRepository;
//    private final PBKDF2Encoder passwordEncoder;
    private final SecurityService service;

    @GetMapping
    public ResponseEntity<Flux<UserDto>> findAll() {
        List<UserDto> userDtos = userService.findAll();

        if (userDtos.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(Flux.empty());
        }

        List<User> users = UserDto.to(userDtos);

        return ResponseEntity.ok(Flux.fromIterable(users).map(UserDto::from));
    }

    @GetMapping("/{userId}")
    public Mono<ResponseEntity<UserDto>> getById(@PathVariable("userId") Integer userId) {

        try {
            UserDto byId = userService.getById(userId);
            return Mono.just(ResponseEntity.ok(byId)).defaultIfEmpty(ResponseEntity.notFound().build());
        } catch (IllegalArgumentException e) {
            return Mono.just(ResponseEntity.notFound().build());
//            return Mono.error(e);
        }

    }

    @GetMapping("/elsethrow/{userId}")
    public Mono<UserDto> getByIdElseThrowMethod(@PathVariable("userId") Integer userId) {

        //TODO должен работать если нет обработки ошибки через elseThrow
        return Mono.just(userService.getById(userId))
                .onErrorResume(e -> Mono.error(new RuntimeException(e.getMessage())));
    }


    @GetMapping("/{userId}/null_test")
    public Mono<ResponseEntity<User>> getByIdWithNull(@PathVariable("userId") Integer userId) {

        User byIdWithNull = userService.getByIdWithNull(userId);

        if (byIdWithNull != null) {
            return Mono.just(ResponseEntity.ok(byIdWithNull));
        } else {
            return Mono.just(ResponseEntity.notFound().build());
        }

        //TODO не работает так как Mono.just(null) не может быть
//        return Mono.just(userService.getByIdWithNull(userId))
//                .map(user -> ResponseEntity.ok(user))
//                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PutMapping("/{userId}")
    public Mono<ResponseEntity<User>> update(@PathVariable("userId") Integer userId, @RequestBody UserDto userDto) {
        if (!userService.isUserExist(userId)) {
            return Mono.just(ResponseEntity.status(HttpStatus.NOT_FOUND).body(null));
        } else {
            return Mono.just(userService.update(userId, userDto)).map(updatedUser -> ResponseEntity.ok(UserDto.to(updatedUser)));
        }
    }

    @PutMapping("/{userId}/update_test")
    public Mono<ResponseEntity<User>> updateSecondType(@PathVariable("userId") Integer userId, @RequestBody UserDto userDto) {
        if (!userService.isUserExist(userId)) {
            return Mono.just(ResponseEntity.status(HttpStatus.NOT_FOUND).body(null));
        } else {
            return Mono.just(userService.getById(userId))
                    .flatMap(user -> {
                        user.setName(userDto.getName());
                        user.setStatus(userDto.getStatus());
                        List<Event> events = userDto.getEvents();
                        user.setEvents(events);
                        return Mono.just(userService.update(userId, user));
                    })
                    .map(updatedUser -> ResponseEntity.ok(UserDto.to(updatedUser)))
                    .onErrorReturn(ResponseEntity.status(HttpStatus.FORBIDDEN).body(null));
        }
    }


    @DeleteMapping("/{userId}")
    public Mono<ResponseEntity<Void>> delete(@PathVariable("userId") Integer userId) {
        //TODO рабочий вариант когда возвращаться из сервиса будет Mono<Void>
//        return userService.findById(user.getId())
//                .flatMap(ourUser -> userService
//                        .delete(ourUser)
//                        .then(Mono.just(ResponseEntity.ok().<Void>build())))
//                .defaultIfEmpty(ResponseEntity.notFound().build());

        if (!userService.isUserExist(userId)) {
            return Mono.just(ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null));
        } else {
            userService.delete(userId);
            return Mono.just(ResponseEntity.ok().body(null));
        }

    }

    @DeleteMapping("/{userId}/or")
    public Mono<ResponseEntity<Void>> testingOrMethod(@PathVariable("userId") Integer userId) {

        if (!userService.isUserExist(userId) | userId.equals(14)) {
            return Mono.just(ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null));
        } else {
            userService.delete(userId);
            return Mono.just(ResponseEntity.ok().body(null));
        }

    }



    //проверка работы с Optional.get() (вроде работает)
//    @GetMapping("/username")
//    public Mono<Object> getByUsername(@RequestBody CheckForm form) {
//       return Mono.just(userRepository.findByUsername(form.getUsername()))
//                .flatMap(user -> {
//                    if (user.isPresent()) {
//                        User existedUser = user.get();
//                        if (!existedUser.getStatus().equals(Status.ACTIVE)) {
//                            return Mono.error(new AuthException("Account disabled", "DISABLED"));
//                        }
//                        return Mono.just(service.generateToken(existedUser).toBuilder().userId(Long.valueOf(existedUser.getId())).build());
//                    }
//                    return Mono.empty();
//                });
//    }

    //проверка работы с Optional.findById() + filter()
    @GetMapping("/find/{id}")
    public Mono<?> findByIdPlusFilter(@PathVariable("id") Integer id) {

        return Mono.just(userRepository.findById(id).stream().filter(user -> user.getStatus().equals(Status.ACTIVE)).findFirst())
                .flatMap(o -> o.isPresent() ? Mono.just(o.get()) : Mono.empty())
                //TODO вопрос
                //этот вариант не выбрасывает в консоль, а постмане код ошибки выходит а вот сообщение нет, почему?
                .switchIfEmpty(Mono.error(new ApiException("User not found", HttpStatus.NOT_FOUND.toString())));
        //этот вариант выбрасывает ошибку в консоль
//                .switchIfEmpty(Mono.error(new ApiException("User not found", HttpStatus.NOT_FOUND.toString())));

    }
}
