//package ru.spring.controller;
//
//import lombok.RequiredArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.BadCredentialsException;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import ru.spring.dto.AuthenticationRequestDto;
//import ru.spring.model.User;
//import ru.spring.repository.UserRepository;
//import ru.spring.security.teamgetter.jwt.JwtTokenProvider;
//import ru.spring.service.UserService;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Map;
//
//import static org.springframework.http.ResponseEntity.ok;
//import static ru.spring.utils.constants.ConfigConstants.ROLE_ADMIN;
//import static ru.spring.utils.constants.ConfigConstants.ROLE_USER;
//import static ru.spring.utils.constants.ControllerConstants.*;
//
//@Slf4j
//@RestController
//@RequiredArgsConstructor
//@RequestMapping(value = "/api/v1/auth")
//public class AuthenticationController {
//
//
//    private final AuthenticationManager authenticationManager;
//    private final UserService userService;
//    private final JwtTokenProvider jwtTokenProvider;
//    private final UserRepository userRepository;
//
//    @PostMapping(value = "/login")
//    public ResponseEntity<Map<Object, Object>> createAuthenticationToken(@RequestBody AuthenticationRequestDto authenticationRequest) {
//        try {
//            String username = authenticationRequest.getUsername();
//            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, authenticationRequest.getPassword()));
//            User user = userService.findUserByUsername(username);
//
//            if (user == null) {
//                throw new UsernameNotFoundException(USER_WITH_USERNAME + username + NOT_FOUND);
//            }
//
//            String role = user.getRole().toString();
//            String returnRole;
//
//            if (role.contains(ROLE_ADMIN_FULL)) {
//                returnRole = ROLE_ADMIN;
//            } else {
//                returnRole = ROLE_USER;
//            }
//
//            String token = jwtTokenProvider.createToken(user);
//            user.setToken(token);
//            userRepository.save(user);
//
//            Map<Object, Object> model = new HashMap<>();
//            model.put(USER_NAME_IN_LINE, username);
//            model.put(TOKEN, token);
//            model.put(ROLE, returnRole);
//            return ok(model);
//        } catch (AuthenticationException e) {
//            throw new BadCredentialsException(INVALID_USERNAME_PASSWORD_SUPPLIED);
//        }
//    }
//
//}
