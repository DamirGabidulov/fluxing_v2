package ru.spring.dto;

import lombok.Builder;
import lombok.Data;
import ru.spring.model.Role;

@Data
@Builder
public class UserResponse {
    private Integer id;
    private String name;
    private String username;
    private Role role;
}
