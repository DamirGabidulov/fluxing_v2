package ru.spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.spring.model.Status;
import ru.spring.model.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {
    Optional<User> findUserByStatusEqualsAndId(Status status, Integer userId);
    List<User> findAllByStatusEquals(Status status);

    Optional<User> findUserByToken(String username);

    Optional<User> findByUsername(String userName);
}
