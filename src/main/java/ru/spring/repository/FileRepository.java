package ru.spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.spring.model.File;

public interface FileRepository extends JpaRepository<File, Integer> {
}
