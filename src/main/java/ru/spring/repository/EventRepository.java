package ru.spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.spring.model.Event;

public interface EventRepository extends JpaRepository<Event, Integer> {
}
