package ru.spring.security.flux;

import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import ru.spring.exception.UnauthorizedException;
import ru.spring.model.Role;
import ru.spring.model.Status;
import ru.spring.repository.UserRepository;

@Component
@RequiredArgsConstructor
public class AuthenticationManager implements ReactiveAuthenticationManager {

    private final UserRepository userRepository;

    @Override
    public Mono<Authentication> authenticate(Authentication authentication) {
        CustomPrincipal principal = (CustomPrincipal) authentication.getPrincipal();
        return Mono.just(userRepository.findById(principal.getId()).stream().filter(user -> user.getStatus().equals(Status.ACTIVE)))
                .switchIfEmpty(Mono.error(new UnauthorizedException("User disabled")))
                .map(user -> authentication);
    }
}
