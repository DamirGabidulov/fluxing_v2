package ru.spring.security.flux;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import ru.spring.exception.AuthException;
import ru.spring.model.Status;
import ru.spring.model.User;
import ru.spring.repository.UserRepository;

import java.security.Signature;
import java.util.*;

@Component
@RequiredArgsConstructor
public class SecurityService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    @Value("${jwt.secret}")
    private String secret;
    @Value("${jwt.expiration}")
    private Integer expirationInSeconds;
    @Value("${jwt.issuer}")
    private String issuer;

    public TokenDetails generateToken(User user){
        Map<String, Object> claims = new HashMap<>() {{
            put("role", user.getRole());
            put("username", user.getUsername());
        }};
                return generateToken(claims, user.getId().toString());
    }

    private TokenDetails generateToken(Map<String, Object> claims, String subject){
        Long expirationTimeInMillis = expirationInSeconds * 1000L;
        Date expirationDate = new Date(new Date().getTime() + expirationTimeInMillis);
        return generateToken(expirationDate, claims, subject);
    }

    private TokenDetails generateToken(Date expirationDate, Map<String, Object> claims, String subject){
        Date createdDate = new Date();
        String token = Jwts.builder()
                .setClaims(claims)
                .setIssuer(issuer)
                .setSubject(subject)
                .setIssuedAt(createdDate)
                .setId(UUID.randomUUID().toString())
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS256, Base64.getEncoder().encodeToString(secret.getBytes()))
                .compact();

        return TokenDetails.builder()
                .token(token)
                .issuedAt(createdDate)
                .expiresAt(expirationDate)
                .build();
    }

    public Mono<TokenDetails> authenticate(String username, String password){
        return Mono.just(userRepository.findByUsername(username))
                .flatMap(user -> {
                    if (user.isPresent()){
                        User existedUser = user.get();
                        if (!existedUser.getStatus().equals(Status.ACTIVE)){
                            return Mono.error(new AuthException("Account disabled", "DISABLED"));
                        }
                        if (!passwordEncoder.matches(password, existedUser.getPassword())){
                            return Mono.error(new AuthException("Invalid password", "INVALID_PASSWORD"));
                        }
                        //удобно сразу закидывать айди юзера в tokenDetails
                        return Mono.just(generateToken(existedUser).toBuilder().userId(Long.valueOf(existedUser.getId())).build());
                    }
                    return Mono.empty();
                })
                .switchIfEmpty(Mono.error(new AuthException("Invalid username", "INVALID_USERNAME")));
    }

}
