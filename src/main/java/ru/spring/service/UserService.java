package ru.spring.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.spring.dto.SignUpForm;
import ru.spring.dto.UserDto;
import ru.spring.dto.UserResponse;
import ru.spring.model.Role;
import ru.spring.model.Status;
import ru.spring.model.User;
import ru.spring.repository.UserRepository;

import java.util.List;
import java.util.NoSuchElementException;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public UserResponse save(SignUpForm form) {
        if (userRepository.findByUsername(form.getUsername()).isPresent()){
            throw new IllegalArgumentException("Пользователь с таким username уже зарегистрирован!");
        }


        User user = User.builder()
                .name(form.getName())
                .username(form.getUsername())
                .password(passwordEncoder.encode(form.getPassword()))
                .status(Status.ACTIVE)
                .build();

        //Добавляем роль для пользователя
        if (form.getRole() == null){
            form.setRole(Role.USER);
        }
        user.setRole(form.getRole());
        userRepository.save(user);

        return UserResponse.builder()
                .id(user.getId())
                .name(user.getName())
                .username(user.getUsername())
                .role(user.getRole())
                .build();
    }

    public boolean isUserExist(Integer userId) {
        return userRepository.findUserByStatusEqualsAndId(Status.ACTIVE, userId).isPresent();
    }

    public UserDto getById(Integer userId) {
        return UserDto.from(userRepository.findUserByStatusEqualsAndId(Status.ACTIVE, userId).orElseThrow(() -> new IllegalArgumentException("User with this id doesn't exist")));
    }

    public User findUserByUsername(String username){
        return userRepository.findByUsername(username).orElseThrow(() -> new IllegalArgumentException("User with this username doesn't exist"));
    }

    public User getByIdWithNull(Integer userId) {
        return userRepository.findUserByStatusEqualsAndId(Status.ACTIVE, userId).orElse(null);
    }

    public List<UserDto> findAll() {
        return UserDto.from(userRepository.findAllByStatusEquals(Status.ACTIVE));
    }

    public UserDto update(Integer userId, UserDto userDto) {
//        if (isUserExist(userId)) {
        if (userId.equals(15)) {
            User user = User.builder()
                    .id(userDto.getId())
                    .name(userDto.getName())
                    .status(userDto.getStatus())
                    .events(userDto.getEvents())
                    .build();
            return UserDto.from(userRepository.save(user));
        } else {
            throw new NoSuchElementException("User with this id doesn't exist");
        }
    }

    public boolean isFileOwnedByUser(Integer userId, Integer fileId) {
        try {
            User user = userRepository
                    .findUserByStatusEqualsAndId(Status.ACTIVE, userId)
                    .orElseThrow(() -> new IllegalArgumentException("User with this id doesn't exist"));
            return user.getEvents().stream()
                    .filter(event -> event.getStatus().equals(Status.ACTIVE))
                    .anyMatch(x -> x.getFile().getId().equals(fileId));
        } catch (IllegalArgumentException e){
            //в контроллере уже Response обрабатывает в зависимости от false/true в данном методе
            return false;
        }
    }

    public boolean isFileWithThisNameAlreadyExist(MultipartFile file, Integer userId) {
        User user = userRepository
                .findUserByStatusEqualsAndId(Status.ACTIVE, userId)
                .orElseThrow(() -> new IllegalArgumentException("User with this id doesn't exist"));
        return user.getEvents().stream().anyMatch(event -> event.getFile().getName().equals(file.getOriginalFilename()));
    }

    public void delete(Integer userId) {
        User user = userRepository.findUserByStatusEqualsAndId(Status.ACTIVE, userId)
                .orElseThrow(() -> new IllegalArgumentException("User with this id doesn't exist"));
        user.setStatus(Status.DELETED);
        userRepository.save(user);
    }
}
