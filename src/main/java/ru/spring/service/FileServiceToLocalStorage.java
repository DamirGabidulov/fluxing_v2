package ru.spring.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.spring.dto.FileDto;
import ru.spring.model.File;
import ru.spring.model.Status;
import ru.spring.repository.FileRepository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FileServiceToLocalStorage {
    private final FileRepository fileRepository;

    public List<File> findAll() {
        return fileRepository.findAll();
    }


    public FileDto save(MultipartFile file) {
        Path directory = Path.of("storage");

        try {
            if (Files.notExists(directory)) {
                Files.createDirectory(directory);
            }
            Files.copy(file.getInputStream(), directory.resolve(file.getOriginalFilename()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        File fileData = File.builder()
                .name(file.getOriginalFilename())
                .location(directory + "\\" + file.getOriginalFilename())
                .status(Status.ACTIVE)
                .build();
        File createdFile = fileRepository.save(fileData);

        return FileDto.from(createdFile);

    }
}
