package ru.spring.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.S3Object;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.File;

@Slf4j
@Service
@RequiredArgsConstructor
public class S3Service {
    private final AmazonS3 s3client;
    private static final String BUCKET_NAME = "spring.data.peal4line";

    public S3Object getS3Object(String location){
        return s3client.getObject(BUCKET_NAME, location);
    }

    public void putS3Object(String location, File physicalFile){
        s3client.putObject(
                BUCKET_NAME,
                location,
                physicalFile);
    }

    public void deleteS3Object(String location){
        s3client.deleteObject(BUCKET_NAME, location);
    }

}
