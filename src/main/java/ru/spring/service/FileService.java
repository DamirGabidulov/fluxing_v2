package ru.spring.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.spring.dto.EventDto;
import ru.spring.dto.FileDto;
import ru.spring.dto.UserDto;
import ru.spring.model.File;
import ru.spring.model.Status;
import ru.spring.model.User;
import ru.spring.repository.FileRepository;
import ru.spring.utils.FileUtil;

import java.io.IOException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FileService {
    private final FileRepository fileRepository;
   private final S3Service s3Service;
   private final UserService userService;
   private final EventService eventService;

    public List<File> findAll() {
        return fileRepository.findAll();
    }

    public FileDto save(MultipartFile file, Integer userId) {
        String location;
        UserDto userDto = userService.getById(userId);
        User user = User.builder()
                .id(userDto.getId())
                .name(userDto.getName())
                .events(userDto.getEvents())
                .status(userDto.getStatus())
                .build();

        try {
            java.io.File physicalFile = FileUtil.multipartToFile(file, file.getOriginalFilename());
            location = "id" + userDto.getId() + "/" + file.getOriginalFilename();
            s3Service.putS3Object(location, physicalFile);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        File fileData = File.builder()
                .name(file.getOriginalFilename())
                .location(location)
                .status(Status.ACTIVE)
                .build();
        File createdFile = fileRepository.save(fileData);

        EventDto eventDto = EventDto.builder()
                .file(createdFile)
                .user(user)
                .status(Status.ACTIVE)
                .build();
        eventService.save(eventDto);

        return FileDto.from(createdFile);
    }

    public FileDto getById(Integer fileId){
        return FileDto.from(fileRepository.findById(fileId).orElseThrow(() -> new IllegalArgumentException("File with this id doesn't exist")));
    }

    public FileDto update(Integer fileDtoId, MultipartFile file, Integer userId) {
        File existedFile = fileRepository.findById(fileDtoId).orElseThrow(() -> new IllegalArgumentException("File with this id doesn't exist"));
        String location;
        try {
            java.io.File physicalFile = FileUtil.multipartToFile(file, existedFile.getName());
            location = "id" + userId + "/" + file.getOriginalFilename();
            s3Service.putS3Object(location, physicalFile);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        if (!file.getOriginalFilename().equals(existedFile.getName())){
            //нужно удалять файл из s3 так как при смене имени файла амазон добавляет новый файл, а старый там так и остается уже не нужный
            s3Service.deleteS3Object(existedFile.getLocation());
        }
        existedFile.setName(file.getOriginalFilename());
        existedFile.setLocation(location);
        return FileDto.from(fileRepository.save(existedFile));
    }

    public void delete(Integer fileDtoId, Integer userId) {
        File existedFile = fileRepository.findById(fileDtoId).orElseThrow(() -> new IllegalArgumentException("File with this id doesn't exist"));
        s3Service.deleteS3Object(existedFile.getLocation());
        //меняю статус у Event тоже
        userService.getById(userId).getEvents().stream()
                .filter(event -> event.getFile().getId().equals(fileDtoId))
                .findFirst()
                .get()
                .setStatus(Status.DELETED);
        existedFile.setStatus(Status.DELETED);
        fileRepository.save(existedFile);
    }
}
