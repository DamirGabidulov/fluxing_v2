package ru.spring.model;

public enum Status {
    ACTIVE, DELETED;
}
